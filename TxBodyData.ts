import { DeployParams, NftChangeOwnerParams, NftCollectionDeployParams, NftItemDeployParams, NftSaleCancelParams, NftSalePlaceGetgemsParams, NftSalePlaceParams, NftSingleDeployParams, NftTransferParams, SignRawParams, TxRequestBody, TxResponseOptions } from "./TxRequest.types";
import { getTimeSec } from "./utils";

export const getSignRawBody = (
  response_options: TxResponseOptions,
  expires_sec: number,
  params: any
): TxRequestBody<SignRawParams> => ({
  type: 'sign-raw-payload',
  default: {
    source: 'EQD2NmD_lH5f5u1Kj3KfGyTvhZSX0Eg6qp2a5IQUKXxOG21n',
    valid_until: getTimeSec() + (50 * 60), // valid 50 minutes
    messages: [
      {
        "address": "EQD2NmD_lH5f5u1Kj3KfGyTvhZSX0Eg6qp2a5IQUKXxOG21n",
        "amount": "100000000",
      },
      {
        "address": "EQD2NmD_lH5f5u1Kj3KfGyTvhZSX0Eg6qp2a5IQUKXxOG21n",
        "amount": "10000000",
        'payload': 'te6ccsEBAQEADgAAABgAAAAAQ29tbWVudCE07Pl9'
      }
    ]
  },
  params,
  response_options,
  expires_sec,
});

export const getNFTCollectionDeployBody = (
  response_options: TxResponseOptions,
  expires_sec: number,
  params: any
): TxRequestBody<NftCollectionDeployParams> => ({
  type: 'nft-collection-deploy',
  default: {
    "ownerAddress": "EQD2NmD_lH5f5u1Kj3KfGyTvhZSX0Eg6qp2a5IQUKXxOG21n",
    "royalty": 0.1,
    "royaltyAddress": "EQD2NmD_lH5f5u1Kj3KfGyTvhZSX0Eg6qp2a5IQUKXxOG21n",
    "collectionContentUri": "https://s.getgems.io/nft/c/628f9800640fb688b8283a49/meta.json",
    "nftItemContentBaseUri": "https://s.getgems.io/nft/c/628f9800640fb688b8283a49/",
    "nftCollectionStateInitHex": "b5ee9c72410227010004c50002013413010353801ec6cc1ff28fcbfcdda951ee53e3649df0b292fa09075553b35c9082852f89c3600000000000000010100302004b000a0064801ec6cc1ff28fcbfcdda951ee53e3649df0b292fa09075553b35c9082852f89c3700114ff00f4a413f4bcf2c80b0402016206050009a11f9fe0050202ce0a070201200908001d00f232cfd633c58073c5b3327b5520003b3b513434cffe900835d27080269fc07e90350c04090408f80c1c165b5b600201200c0b00113e910c1c2ebcb8536002d70c8871c02497c0f83434c0c05c6c2497c0f83e903e900c7e800c5c75c87e800c7e800c3c00812ce3850c1b088d148cb1c17cb865407e90350c0408fc00f801b4c7f4cfe08417f30f45148c2ea3a1cc840dd78c9004f80c0d0d0d4d60840bf2c9a884aeb8c097c12103fcbc200e0d00727082108b77173505c8cbff5004cf1610248040708010c8cb055007cf165005fa0215cb6a12cb1fcb3f226eb39458cf17019132e201c901fb0001f65135c705f2e191fa4021f001fa40d20031fa00820afaf0801ba121945315a0a1de22d70b01c300209206a19136e220c2fff2e192218e3e821005138d91c85009cf16500bcf16712449145446a0708010c8cb055007cf165005fa0215cb6a12cb1fcb3f226eb39458cf17019132e201c901fb00104794102a375be20f0082028e3526f0018210d53276db103744006d71708010c8cb055007cf165005fa0215cb6a12cb1fcb3f226eb39458cf17019132e201c901fb0093303234e25502f00302001211006868747470733a2f2f732e67657467656d732e696f2f6e66742f632f3632386639393061343334356261366565623337623038332f007c0168747470733a2f2f732e67657467656d732e696f2f6e66742f632f3632386639393061343334356261366565623337623038332f6d6574612e6a736f6e0114ff00f4a413f4bcf2c80b140201621c1502012017160025bc82df6a2687d20699fea6a6a182de86a182c40201201b180201201a19002db4f47da89a1f481a67fa9a9a86028be09e008e003e00b0002fb5dafda89a1f481a67fa9a9a860d883a1a61fa61ff4806100043b8b5d31ed44d0fa40d33fd4d4d43010245f04d0d431d430d071c8cb0701cf16ccc980202cd221d0201201f1e003d45af0047021f005778018c8cb0558cf165004fa0213cb6b12ccccc971fb0080201202120001b3e401d3232c084b281f2fff27420002d007232cffe0a33c5b25c083232c044fd003d0032c0326004e7d10638048adf000e8698180b8d848adf07d201800e98fe99ff6a2687d20699fea6a6a184108349e9ca829405d47141baf8280e8410854658056b84008646582a802e78b127d010a65b509e58fe59f80e78b64c0207d80701b28b9e382f970c892e000f18112e001718112e001f181181981e002426252423003c8e15d4d43010344130c85005cf1613cb3fccccccc9ed54e05f04840ff2f0002c323401fa40304144c85005cf1613cb3fccccccc9ed5400a6357003d4308e378040f4966fa5208e2906a4208100fabe93f2c18fde81019321a05325bbf2f402fa00d43022544b30f00623ba9302a402de04926c21e2b3e6303250444313c85005cf1613cb3fccccccc9ed5400603502d33f5313bbf2e1925313ba01fa00d43028103459f0068e1201a44343c85005cf1613cb3fccccccc9ed54925f05e24d391a89",
    "amount": "100000000",
    "contractAddress": "EQB2LZVUlK3LfU1On-8g_iQWs8tWofBxQGzcKKYTs-QXJl1g",
    "nftItemCodeHex": "b5ee9c72410227010004c50002013413010353801ec6cc1ff28fcbfcdd",
  },
  params,
  response_options,
  expires_sec,
});

export const getNFTItemDeployBody = (
  response_options: TxResponseOptions,
  expires_sec: number,
  params: any
): TxRequestBody<NftItemDeployParams> => ({
  type: 'nft-item-deploy',
  default: {
    "ownerAddress": "EQD2NmD_lH5f5u1Kj3KfGyTvhZSX0Eg6qp2a5IQUKXxOG21n",
    "itemContentUri": "0.json",
    "nftCollectionAddress": "EQB2LZVUlK3LfU1On-8g_iQWs8tWofBxQGzcKKYTs-QXJl1g",
    "itemIndex": 0,
    "amount": "100000000",
    "forwardAmount": "50000000",
    'nftItemContentBaseUri': 'https://s.getgems.io/nft/c/628f990a4345ba6eeb37b083/',    
  },
  params,
  response_options,
  expires_sec,
});

export const getNFTChangeOwnerBody = (
  response_options: TxResponseOptions,
  expires_sec: number,
  params: any
): TxRequestBody<NftChangeOwnerParams> => ({
  type: 'nft-change-owner',
  default: {
    newOwnerAddress: 'EQAn7UCXbrjmgAApFV5FzuVX4P2avn_S3O3BwFpxgi2yf_Cy',
    nftCollectionAddress: 'EQAiwgIKpPUggGpVkiJe_Wo0flouYiAuipM78qgTCutTm3-g',
    amount: '100000000',
  },
  params,
  response_options,
  expires_sec,
});

export const getNFTTransferBody = (
  response_options: TxResponseOptions,
  expires_sec: number,
  params: any
): TxRequestBody<NftTransferParams> => ({
  type: 'nft-transfer',
  default: {
    newOwnerAddress: 'EQAn7UCXbrjmgAApFV5FzuVX4P2avn_S3O3BwFpxgi2yf_Cy',
    nftItemAddress: 'EQBWPunGskn1VravuYLccv1Hn4hnTThBmMl01KFaCKk9Vn3j',
    amount: '100000000',
    forwardAmount: '20000000',
    text: 'just message',
  },
  params,
  response_options,
  expires_sec,
});

export const getNFTSalePlaceBody = (
  response_options: TxResponseOptions,
  expires_sec: number,
  params: any
): TxRequestBody<NftSalePlaceParams> => ({
  type: 'nft-sale-place',
  default: {
    marketplaceAddress: 'EQD2NmD_lH5f5u1Kj3KfGyTvhZSX0Eg6qp2a5IQUKXxOG21n',
    nftItemAddress: 'EQCu-GX7Gq0Q5WXKKQWLpwOw3ccjSrhAo6l4sffZJLH94mGC',
    fullPrice: '1000000000',
    marketplaceFee: '100000000',
    royaltyAddress: 'EQD2NmD_lH5f5u1Kj3KfGyTvhZSX0Eg6qp2a5IQUKXxOG21n',
    royaltyAmount: '100000000',
    amount: '100000000',
  },
  params,
  response_options,
  expires_sec,
});

export const getNFTSaleCancelBody = (
  response_options: TxResponseOptions,
  expires_sec: number,
  params: any
): TxRequestBody<NftSaleCancelParams> => ({
  type: 'nft-sale-cancel',
  default: {
    "nftItemAddress": "EQBWPunGskn1VravuYLccv1Hn4hnTThBmMl01KFaCKk9Vn3j",
    "saleAddress": "EQDSvW4yebxQup3j9tdAfjwhdGuZDb1C8lfYV4z3O7mLFb7D",
    "ownerAddress": "EQD2NmD_lH5f5u1Kj3KfGyTvhZSX0Eg6qp2a5IQUKXxOG21n",
    "amount": "1100000000",
  },
  params,
  response_options,
  expires_sec,
});

export const getGetgemsNFTSaleBody = (
  response_options: TxResponseOptions,
  expires_sec: number,
  params: any
): TxRequestBody<NftSalePlaceGetgemsParams> => ({
  type: 'nft-sale-place-getgems',
  default: {
    "marketplaceFeeAddress": "EQBYTuYbLf8INxFtD8tQeNk5ZLy-nAX9ahQbG_yl1qQ-GEMS",
    "marketplaceFee": "50000000",
    "royaltyAddress": "EQD2NmD_lH5f5u1Kj3KfGyTvhZSX0Eg6qp2a5IQUKXxOG21n",
    "royaltyAmount": "10000000",
    "createdAt": 1653498738,
    "marketplaceAddress": "EQBYTuYbLf8INxFtD8tQeNk5ZLy-nAX9ahQbG_yl1qQ-GEMS",
    "nftItemAddress": "EQA0S3vSblu2GbZ0ODSIVgneKEDqvvEvrXF2aoslaObITPU9",
    "ownerAddress": "EQD2NmD_lH5f5u1Kj3KfGyTvhZSX0Eg6qp2a5IQUKXxOG21n",
    "fullPrice": "1000000000",
    "deployAmount": "200000000",
    "transferAmount": "200000000",
    "saleMessageBocHex": "b5ee9c7241021101000314000288000000015de17c4aa8997cc1e4a953131818e5422f0e4368579abc4f9a0666c6f80470742f58c2fb0050765107562960443b4baa72a0e76942bf69102b9ab7b6d1f329070201000002013405030197314731b9400584ee61b2dff0837116d0fcb5078d93964bcbe9c05fd6a141b1bfca5d6a43e188006896f7a4dcb76c336ce8706910ac13bc5081d57de25f5ae2ecd5164ad1cd909821dcd650040400958014726b0c3ef3b5eb3427ada305c2c80421805f31c7be31fb4e971eb5628357e30805f5e101003d8d983fe51f97f9bb52a3dca7c6c93be16525f4120eaaa766b921050a5f1386ce625a020114ff00f4a413f4bcf2c80b0602012008070004f2300201480a090051a03859da89a1a601a63ff481f481f481f401a861a1f481f401f481f4006104208c92b0a0158002ab010202cd0d0b01f7660840ee6b280149828148c2fbcb87089343e903e803e903e800c14e4a848685421e845a814a41c20043232c15400f3c5807e80b2dab25c7ec00970800975d27080ac2385d4115c20043232c15400f3c5807e80b2dab25c7ec00408e48d0d38969c20043232c15400f3c5807e80b2dab25c7ec01c08208417f30f4520c0082218018c8cb052acf1621fa02cb6acb1f13cb3f23cf165003cf16ca0021fa02ca00c98306fb0071555006c8cb0015cb1f5003cf1601cf1601cf1601fa02ccc9ed5402f7d00e8698180b8d8492f82707d201876a2686980698ffd207d207d207d006a18136000f968ca116ba4e10159c720191c1c29a0e382c92f847028a26382f970fa02698fc1080289c6c8895d7970fae99f98fd2018202b036465800ae58fa801e78b00e78b00e78b00fd016664f6aa701b13e380718103e98fe99f9810c100e014ac001925f0be021c0029f31104910384760102510241023f005e03ac003e3025f09840ff2f00f00ca82103b9aca0018bef2e1c95346c7055152c70515b1f2e1ca702082105fcc3d14218010c8cb0528cf1621fa02cb6acb1f19cb3f27cf1627cf1618ca0027fa0217ca00c98040fb0071065044451506c8cb0015cb1f5003cf1601cf1601cf1601fa02ccc9ed540016371038476514433070f0059bcd46bb",
    "marketplaceSignatureHex": "5de17c4aa8997cc1e4a953131818e5422f0e4368579abc4f9a0666c6f80470742f58c2fb0050765107562960443b4baa72a0e76942bf69102b9ab7b6d1f32907",
    "forwardAmount": "200000000",
  },
  params,
  response_options,
  expires_sec,
});


export const getNftSingleDeployBody = (
  response_options: TxResponseOptions,
  expires_sec: number,
  params: any
): TxRequestBody<NftSingleDeployParams> => ({
  type: 'nft-single-deploy',
  default: {
    "contractAddress": "EQACwW3us-Pc4NRiSa3TZF0s0Ujh8N3tssj7luR07MsJILKJ",
    "itemContentUri": "https://s.getgems.io/nft/s/628f/628fa3924345ba6eeb37b08a/meta.json",
    "stateInitHex": "b5ee9c72410219010003c30002013404010285801ec6cc1ff28fcbfcdda951ee53e3649df0b292fa09075553b35c9082852f89c37003d8d983fe51f97f9bb52a3dca7c6c93be16525f4120eaaa766b921050a5f1386e0302004b00010064801ec6cc1ff28fcbfcdda951ee53e3649df0b292fa09075553b35c9082852f89c37000860168747470733a2f2f732e67657467656d732e696f2f6e66742f732f363238662f3632386661333932343334356261366565623337623038612f6d6574612e6a736f6e0114ff00f4a413f4bcf2c80b050201620b0602012008070023bc7e7f8011818b8646580e4bfb801682021c0201580a090011b40e9e0042046be070001db5dafe004d863a1a61fa61ff4806100202ce0f0c0201200e0d001b32140133c59633c5b333327b552000153b51343e903e9035350c20020120111000113e910c1c2ebcb8536004b90c8871c02497c0f83434c0c05c6c2497c0f83e903e900c7e800c5c75c87e800c7e800c3c0081b4c7f4cfe08417f30f45148c2ea3a1cc8411c40d90057820840bf2c9a8948c2eb8c0a0841a4f4e54148c2eb8c0a0840701104a948c2ea017161512015c8e8732104710364015e0313234353582101a0b9d5112ba9f5113c705f2e19a01d4d4301023f003e05f04840ff2f01301f65136c705f2e191fa4021f001fa40d20031fa00820afaf0801ba121945315a0a1de22d70b01c300209206a19136e220c2fff2e192218e3e8210511a4463c8500acf16500bcf1671244a145446b0708010c8cb055007cf165005fa0215cb6a12cb1fcb3f226eb39458cf17019132e201c901fb00105794102a385be2140082028e3526f0018210d53276db103745006d71708010c8cb055007cf165005fa0215cb6a12cb1fcb3f226eb39458cf17019132e201c901fb0093303334e25502f0030054165f063301d0128210a8cb00ad708010c8cb055005cf1624fa0214cb6a13cb1fcb3f01cf16c98040fb000086165f066c2270c8cb01c97082108b77173521c8cbff03d013cf16138040708010c8cb055007cf165005fa0215cb6a12cb1fcb3f226eb39458cf17019132e201c901fb0001f65137c705f2e191fa4021f001fa40d20031fa00820afaf0801ba121945315a0a1de22d70b01c300209206a19136e220c200f2e192218e3e821005138d91c8500bcf16500bcf1671244b145446c0708010c8cb055007cf165005fa0215cb6a12cb1fcb3f226eb39458cf17019132e201c901fb00106794102a395be2180082028e3526f0018210d53276db103746006d71708010c8cb055007cf165005fa0215cb6a12cb1fcb3f226eb39458cf17019132e201c901fb0093303434e25502f0035677cdb2",
    "amount": "100000000",
  },
  params,
  response_options,
  expires_sec,
});

export const getDeployBody = (
  response_options: TxResponseOptions,
  expires_sec: number,
  params: any
): TxRequestBody<DeployParams> => ({
  type: 'deploy',
  default: {
    address: 'EQD0QPfz2s0GRvxYn7mxaDq3Uqn29GgzkuDAC5KIM1tuZhny',
    stateInitHex: '',
    amount: '100000000'
  },
  params,
  response_options,
  expires_sec,
});


export const bodyExtractors = {
  'sign-raw-payload': getSignRawBody,
  'nft-single-deploy': getNftSingleDeployBody,
  'nft-item-deploy': getNFTItemDeployBody,
  'nft-collection-deploy': getNFTCollectionDeployBody,
  'nft-sale-place-getgems': getGetgemsNFTSaleBody,
  'nft-transfer': getNFTTransferBody,
  'nft-sale-cancel': getNFTSaleCancelBody,

  'nft-change-owner': getNFTChangeOwnerBody,
  'nft-sale-place': getNFTSalePlaceBody,
  'deploy': getDeployBody,
};




export const validateBodyParams = (params: any, type: string) => {
  switch (type) {
    case 'nft-single-deploy':
      validateObject(params, [
        'itemContentUri',
        'contractAddress',
        'stateInitHex',
        'amount'
      ]);
      break;
    case 'nft-item-deploy':
      validateObject(params, [
        'nftItemContentBaseUri',
        'nftCollectionAddress',
        'itemContentUri',
        'itemIndex',
        'amount',
        'forwardAmount',
      ]);
      break;
    case 'nft-collection-deploy':
      validateObject(params, [
        'royalty',
        'royaltyAddress',
        'collectionContentUri',
        'nftItemContentBaseUri',
        'nftCollectionStateInitHex',
        'nftItemCodeHex',
        'contractAddress',
        'amount' 
      ]);
      break;
    case 'nft-sale-place-getgems':
      validateObject(params, [
        'marketplaceFeeAddress',
        'marketplaceFee',
        'royaltyAddress',
        'royaltyAmount',
        'createdAt',
        'marketplaceAddress',
        'nftItemAddress', 
        'ownerAddress', 
        'fullPrice', 
        'deployAmount',
        'transferAmount', 
        'saleMessageBocHex', 
        'marketplaceSignatureHex',
        'forwardAmount',
      ]);
      break;
    case 'nft-transfer':
      validateObject(params, [
        'newOwnerAddress',
        'nftItemAddress',
        'forwardAmount',
        'amount'
      ]);
      break;
    case 'nft-sale-cancel': 
      validateObject(params, [
        'nftItemAddress',
        'saleAddress',
        'ownerAddress',
        'amount'
      ]);
      break;
    case 'sign-raw-payload': 
      validateObject(params, [
        'source',
        'valid_until',
        'messages'
      ]);
      break;
  }
}



export function validateObject(obj: Record<string, any>, fields: string[]) {
  const bodyKeys = Object.keys(obj);
  for (let field of fields) {
    if (!bodyKeys.includes(field)) {
      throw new Error(`Field '${field}' is required`)
    }
  }
}