import type { NextPage } from 'next'
import React from 'react'
import styles from '../styles/Home.module.css'
import { bodyExtractors, validateBodyParams } from '../TxBodyData'
import Modal from 'react-modal';
import { getTimeSec, hostname, makeTxInlineDeepLink, makeTxJsonDeepLink, protocol, txExpirationSec } from '../utils'
import vkQr from '@vkontakte/vk-qr';
const beautify = require("json-beautify");

const diff = (a, b) => {
  const res = {};
  const keys = Object.keys(a)
  for (const key of keys) {
    if (a[key] !== b[key] && b[key]) {
      res[key] = b[key];
    }
  }

  return res
}


const Home: NextPage = () => {
  const [isOpen, setIsOpen] = React.useState(false);
  const [modalContent, setModalContent] = React.useState<any>({});

  const expiresSec = Math.floor(getTimeSec() + txExpirationSec);
  const responseOptions = {
    callback_url: `${protocol}://${hostname}/complete`,
    return_url: `${protocol}://${hostname}/complete`,
    broadcast: true
  };

  const deeplinks = React.useMemo(() => {
    return Object.keys(bodyExtractors).map((type) => {
      const extractor = (bodyExtractors as any)[type];
      const body = extractor(responseOptions, expiresSec);

      return {
        type,
        params: body.default,
        inlineLink: makeTxInlineDeepLink(body),
        jsonLink: makeTxJsonDeepLink(`${hostname}/${type}.json`)
      }
    });
  }, []);

  const initialInputs = React.useMemo(() => {
    const acc: Record<string, any> = {};
    return deeplinks.reduce((acc, item) => {
      acc[item.type] = beautify(item.params, null, 2, 100);
      return acc;
    }, acc)
  }, [deeplinks]);

  const [inputs, dispatch] = React.useReducer((state, action) => {
    if (action.type === 'CHANGE') {
      return {
        ...state,
        [action.operation]: action.params
      }
    } 

    return state;
  }, initialInputs)
  

  const makeDeepLink = React.useCallback((type: string) => {
    try {
      const params = JSON.parse(inputs[type]);
      
      const extractor = (bodyExtractors as any)[type];

      const body = extractor(responseOptions, expiresSec, params);

      const inlineLink = makeTxInlineDeepLink(body);

      validateBodyParams(body.params, type);

      const query = Object.keys(params).map(k => `${encodeURIComponent(k)}=${typeof params[k] === 'object' ? encodeURIComponent(JSON.stringify(params[k])) : encodeURIComponent(params[k]) }`).join('&')
      const jsonLink = makeTxJsonDeepLink(`${hostname}/${type}.json?${query}`);

      let qrSvg = '';
      try {
        qrSvg = vkQr.createQR(jsonLink, {
          qrSize: 256,
          isShowLogo: false,
        });
      } catch(err) {}

      setModalContent({ 
        type,
        qr: qrSvg,
        inlineLink,
        jsonLink
      });

      setIsOpen(!isOpen);
    } catch (err) {
      alert(err);
    }
  }, [isOpen, inputs]);

  return (
    <div className={styles.container}>
      <h1 id="link">NFT deep links</h1>
      <div className="wrap">
        {deeplinks.map((item, key) => (
          <div className="item" key={`item-${key}`}>
            <h2>{item.type}</h2>
            <div style={{ display: 'flex', justifyContent: 'center', flexDirection: 'column' }}>
              <textarea 
                className="code"
                value={inputs[item.type]}
                onChange={(e) => 
                  dispatch({ type: 'CHANGE', params: e.target.value, operation: item.type  })
                }
              />
              <br />
              <button onClick={() => makeDeepLink(item.type)}>Make Deeplink</button>
            </div>
          </div>
        ))}

        <Modal
          isOpen={isOpen}
          onRequestClose={() => {}}
          className="mymodal"
          overlayClassName="myoverlay"          
        >
          <div style={{ width: 320, display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }}>
            <h2>{modalContent.type}</h2>
            <div dangerouslySetInnerHTML={{ __html: modalContent.qr }}/>
            
            <a className="link" href={modalContent.inlineLink} >Inline deep link</a>
            <br />
            <a className="link" href={modalContent.jsonLink}>Json deep link</a>
            <br />
            <br />
            <button onClick={() => setIsOpen(false)}>Close</button>
          </div> 
        </Modal>
      </div>
    </div>
  )
}

export default Home
