// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import { bodyExtractors } from '../../TxBodyData';
import { getTimeSec, hostname, protocol, txExpirationSec } from '../../utils';

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const { filename, ...params } = req.query;
  const operation = filename?.[0].replace('.json', '');

  const expiresSec = Math.floor(getTimeSec() + txExpirationSec);
    const responseOptions = {
      callback_url: `${protocol}://${hostname}/complete`,
      return_url: `${protocol}://${hostname}/complete`,
      broadcast: true
    };

    if (operation) {
      const bodyExtractor = (bodyExtractors as any)[operation];
      if (params.createdAt) {
        (params as any).createdAt = Number(params.createdAt);
      }

      if (bodyExtractor) {
        if (params.messages) {
          try {
            const m = JSON.parse((params as any).messages);
            params.messages = m;
          } catch (err) {}
        }

        const body = bodyExtractor(responseOptions, expiresSec, params);

        return res.json({
          version: "0",
          body,
        });
      }
    }

    res.json({ error: true });
}
